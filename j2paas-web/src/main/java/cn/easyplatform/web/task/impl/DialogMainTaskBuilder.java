/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.vos.AbstractPageVo;
import cn.easyplatform.messages.vos.VisibleVo;
import cn.easyplatform.spi.service.TaskService;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.DeviceType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.utils.PageUtils;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Window;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DialogMainTaskBuilder extends AbstractMainTaskBuilder<Window> {

    /**
     * @param container
     * @param taskId
     * @param pv
     */
    public DialogMainTaskBuilder(Component container, String taskId,
                                 AbstractPageVo pv) {
        super(container, taskId, pv);
    }

    @Override
    public void build() {
        super.build();
        idSpace.setTitle(apv.getTitile());
        // Caption caption = new Caption();
        // caption.setLabel(pv.getTitile());
        // PageUtils.setTaskIcon(caption, pv.getImage());
        // caption.setParent(main);
        if (Contexts.getEnv().getDeviceType()
                .equals(DeviceType.MOBILE.getName())) {
            idSpace.setClosable(true);
            VisibleVo vv = null;
            if (apv instanceof VisibleVo) {
                vv = (VisibleVo) apv;
            }
            if (vv != null && (vv.getHeight() > 0 || vv.getWidth() > 0)) {
                setSize();
            } else {
                idSpace.setMaximized(true);
            }
            idSpace.setMaximizable(false);
            idSpace.setBorder("none");
            idSpace.setShadow(false);
            idSpace.setContentSclass("p-0");
            idSpace.doHighlighted();
        } else {
            idSpace.setClosable(true);
            idSpace.setSizable(true);
            idSpace.setMaximizable(true);
            setSize();
            if (apv.getOpenModel() == Constants.OPEN_OVERLAPPED)
                idSpace.doOverlapped();
            else
                idSpace.doHighlighted();
        }
        idSpace.setEvent("close()");
        PageUtils.addEventListener(this, Events.ON_CLOSE, idSpace);
    }

    @Override
    protected void doPage() {
        super.doPage();
        setSize();
    }

    @Override
    protected Window create() {
        return new Window();
    }

    private void setSize() {
        if (apv instanceof VisibleVo) {
            VisibleVo vv = (VisibleVo) apv;
            if (vv.getHeight() > 0)
                idSpace.setHeight(vv.getHeight() + "px");
            if (vv.getWidth() > 0)
                idSpace.setWidth(vv.getWidth() + "px");
            if (vv.getHeight() <= 0 || vv.getWidth() <= 0) {
                idSpace.setMaximizable(true);
                idSpace.setMaximized(true);
            }
            if (Strings.isBlank(vv.getPosition()))
                idSpace.setPosition("center");
            else
                idSpace.setPosition(vv.getPosition());
        } else {
            if (Strings.isBlank(idSpace.getHeight()))
                idSpace.setVflex("1");
            if (Strings.isBlank(idSpace.getWidth()))
                idSpace.setHflex("1");
        }
    }

    @Override
    public void close(boolean normal) {
        if (!normal) {
            IResponseMessage<?> resp = ServiceLocator.lookup(
                    TaskService.class).close(
                    new SimpleRequestMessage(getId(), null));
            if (!resp.isSuccess()) {
                MessageBox.showMessage(resp);
                return;
            }
        }
        WebUtils.removeTask(getId());
        clear();
        idSpace.detach();
        idSpace = null;
    }

}
