/**
 * Copyright 2019 吉鼎科技.
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.ServiceException;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Nums;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.lang.util.AbstractContext;
import cn.easyplatform.lang.util.Context;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.spi.annotation.Service;
import cn.easyplatform.spi.annotation.ServiceType;
import cn.easyplatform.spi.engine.EngineFactory;
import cn.easyplatform.spi.listener.ApplicationListener;
import cn.easyplatform.spi.listener.event.Event;
import cn.easyplatform.spi.service.ApplicationService;
import cn.easyplatform.spi.service.ServiceFactory;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.utils.JmsUtils;
import cn.easyplatform.utils.resource.GResource;
import cn.easyplatform.utils.resource.Ini;
import cn.easyplatform.utils.resource.Scans;
import cn.easyplatform.web.message.MessageEventHandler;
import cn.easyplatform.web.message.MessageEventService;
import cn.easyplatform.web.message.MessageObserver;
import cn.easyplatform.web.message.entity.Destination;
import cn.easyplatform.web.message.impl.*;
import cn.easyplatform.web.service.DefaultServiceFactory;
import cn.easyplatform.web.servlet.WebDesktopListener;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WebApp;
import org.zkoss.zk.ui.util.DesktopCleanup;
import org.zkoss.zk.ui.util.WebAppCleanup;
import org.zkoss.zk.ui.util.WebAppInit;
import org.zkoss.zkmax.au.websocket.WebSocketWebAppInit;

import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class WebApps extends AbstractContext {

    private final static int TIMER_INTERVAL = 30000;

    private Logger LOG;

    /**
     * 单例模式
     */
    private static WebApps one = new WebApps();

    /**
     * webApp对象
     */
    private WebApp webApp;

    /**
     * 服务工厂
     */
    private ServiceFactory serviceFactory;

    /**
     * 应用事件侦听器
     */
    private ApplicationListener applicationListener;

    /**
     * 默认的会话间隔
     */
    private int sessionInterval = TIMER_INTERVAL;

    /**
     * 配置项
     */
    private Ini config;

    /**
     * 定义的应用侦听器
     */
    private List<WebAppCleanup> webAppListeners;

    /**
     * 定义的订阅通道信息
     */
    private Map<String, Destination> jmsDestinations;

    /**
     * jms连接工厂
     */
    private ConnectionFactory connectionFactory;

    /**
     * 消息服务
     */
    private MessageEventService messageEventService;

    /**
     * 消息处理
     */
    private MessageEventHandler messageEventHandler;

    //Web应用前端工作空间($708|$730)
    private String workspace;

    //模板路径
    private String templatePath;

    //私钥
    private PrivateKey privateKey;

    //公钥
    private PublicKey publicKey;

    /**
     * @return
     */
    public static WebApps me() {
        return one;
    }

    /**
     * 获取Web所在的路径
     *
     * @return
     */
    public final static String getRealPath() {
        return one.webApp.getRealPath("");
    }

    /**
     * 获取指定目录所在的路径
     *
     * @return
     */
    public final static String getRealPath(String path) {
        return one.webApp.getRealPath(path);
    }

    /**
     * 获取应用上下文路径
     *
     * @return
     */
    public static String getContextPath() {
        return one.webApp.getServletContext().getContextPath();
    }

    /**
     * 获取客户端ip地址
     *
     * @return
     */
    public static String getRemoteAddr(HttpServletRequest req) {
        // 如果有代理服务器，通过获取头X-Real-IP来获取真实的IP
        String ip = req.getHeader("X-Real-IP");
        if (ip != null)
            return ip;
        ip = req.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = req.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = req.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = req.getRemoteAddr();
        }
        if (ip == null)
            ip = "";
        return ip;
    }

    /**
     * 返回服务器url地址 例如:http://localhost:8080/easyplatform
     *
     * @return
     */
    public final static String getServerUrl() {
        Execution exec = Executions.getCurrent();
        StringBuilder sb = new StringBuilder();
        sb.append(exec.getScheme()).append("://")
                .append(exec.getServerName());
        if (exec.getServerPort() != 80)
            sb.append(":").append(exec.getServerPort());
        sb.append(exec.getContextPath());
        return sb.toString();
    }


    /**
     * @param app
     */
    void start(WebApp app) throws Exception {
        this.webApp = app;
        // 设置easyplatform的工作环境
        Map<String, String> args = new HashMap<String, String>();
        args.put("easyplatform.home", app.getRealPath("WEB-INF"));
        args.put("easyplatform.conf", app.getRealPath("WEB-INF/config"));
        args.put("easyplatform.log", (String) webApp.getAttribute("easyplatform.log"));
        System.setProperty("easyplatform.home", args.get("easyplatform.home"));
        LOG = LoggerFactory.getLogger(WebApps.class);
        File file = new File(app.getRealPath("WEB-INF/config/easyplatform.conf"));
        if (file.exists() && file.isFile()) {
            if (LOG.isDebugEnabled())
                LOG.debug(Labels.getLabel("app.config.load", new Object[]{app.getRealPath("WEB-INF/config/easyplatform.conf")}));
            config = Ini.fromResourcePath(file);
        } else
            throw new Exception(Labels.getLabel("app.config.not.found", new Object[]{app.getRealPath("WEB-INF/config/easyplatform.conf")}));
        // 初始化资源
        Scans.me().initWeb(args.get("easyplatform.home"));
        args.put("web.apps", FilenameUtils.normalizeNoEndSeparator(webApp.getRealPath("")));
        EngineFactory.me().init(args);
        //会话时长
        int sessionTimeout = EngineFactory.me().getSessionTimeout();
        //是否保持会话存活
        boolean sessionKeepAlive = EngineFactory.me().isSessionKeepAlive();
        // 设置会话
        webApp.getConfiguration().setSessionMaxDesktops(-1);
        webApp.getConfiguration().setSessionMaxInactiveInterval(sessionTimeout);
        webApp.getConfiguration().setDesktopMaxInactiveInterval(sessionTimeout);
        webApp.getConfiguration().setTimerKeepAlive(sessionKeepAlive);
        serviceFactory = new DefaultServiceFactory();
        //初始化websocket
        new WebSocketWebAppInit().init(webApp);
        if (EngineFactory.me().checkModel()) {
            startup();
        } else {
            //表示数据库环境未就绪，需要配置
            webApp.setAttribute("MOD_READY", Boolean.FALSE);
        }
    }

    /**
     * 初始化数据库
     *
     * @param properties
     * @param applicationListener
     */
    public void setup(Map<String, String> properties, ApplicationListener applicationListener) {
        EngineFactory.me().setApplicationListener(applicationListener);
        EngineFactory.me().setupModel(properties);
    }

    /**
     * 启动服务
     *
     * @throws Exception
     */
    public void startup() throws Exception {
        List<Class> webappListener = new ArrayList<>();
        EngineFactory.me().start();
        Ini.Section webConfig = config.get("web");
        Ini.Section jmsConfig = config.get("jms");
        if (jmsConfig == null) {
            EngineFactory.me().setApplicationListener(applicationListener = new DefaultApplicationListener());
        } else {
            connectionFactory = JmsUtils.createConnectionFactory(jmsConfig.entrySet());
            applicationListener = new JmsApplicationListener(connectionFactory, new Destination(Constants.EASYPLATFORM_TOPIC));
            if (LOG.isDebugEnabled())
                LOG.debug("Found applicationListener: {}", applicationListener.getClass());
            //如果有定义消息连接工厂才需要配置消息订阅信息
            if (webConfig != null)
                setupJmsDestinations(webConfig, connectionFactory);
        }
        //如果有定义消息连接工厂才需要配置消息订阅信息
        if (webConfig != null) {
            String option = webConfig.get("sessionInterval");
            if (!Strings.isBlank(option))
                sessionInterval = Integer.parseInt(option);
            setupWebAppListener(webConfig, webappListener);
        }

        //获取初始化配置
        ApplicationService as = serviceFactory.lockup(ApplicationService.class);
        IResponseMessage<?> resp = as.init(new SimpleRequestMessage());
        if (resp.isSuccess()) {
            Object[] options = (Object[]) resp.getBody();
            publicKey = (PublicKey) options[0];
            privateKey = (PrivateKey) options[1];
            workspace = (String) options[2];
            templatePath = (String) options[3];
        } else
            throw new Exception(resp.getCode() + ":" + resp.getBody());
        if (Strings.isBlank(workspace))
            workspace = WebApps.getRealPath("apps");
        if (Strings.isBlank(templatePath))
            templatePath = FilenameUtils.normalize(WebApps.getRealPath("WEB-INF/template"));

        //如果小于30秒，重置为默认值
        if (sessionInterval < TIMER_INTERVAL)
            sessionInterval = TIMER_INTERVAL;
        // 增加zk侦听器
        webApp.getConfiguration().addListener(WebDesktopListener.class);
        messageEventService = new MessageEventServiceImpl();
        messageEventService.start(messageEventHandler = new MessageEventHandlerImpl());
        //增加web端listener
        for (Class cls : webappListener) {
            if (WebAppInit.class.isAssignableFrom(cls)) {
                WebAppInit webAppInit = (WebAppInit) cls.newInstance();
                if (WebAppCleanup.class.isAssignableFrom(cls)) {
                    if (webAppListeners == null)
                        webAppListeners = new ArrayList<>();
                    webAppListeners.add((WebAppCleanup) webAppInit);
                }
                webAppInit.init(webApp);
            } else
                webApp.getConfiguration().addListener(cls);
        }
    }

    /**
     * 设置web端侦听器
     *
     * @param webappListener
     * @throws ClassNotFoundException
     */

    private void setupWebAppListener(Ini.Section webConfig, List<Class> webappListener) throws ClassNotFoundException {
        String option = webConfig.get("webappListener");
        if (!Strings.isBlank(option)) {
            String[] listener = option.split(",");
            for (String cls : listener)
                webappListener.add(Lang.loadClass(cls));
        }
    }

    /**
     * 配置消息订阅信息
     */
    private void setupJmsDestinations(Ini.Section webConfig, ConnectionFactory connectionFactory) {
        String destination = "";
        Map<String, Destination> destinations = new HashMap<>();
        for (Map.Entry<String, String> entry : webConfig.entrySet()) {
            if (entry.getKey().startsWith("destination")) {
                if (entry.getKey().endsWith("name")) {
                    destination = StringUtils.substringBeforeLast(entry.getKey(), ".");
                    Destination jmsDestination = new Destination(entry.getValue());
                    if (jmsDestinations == null)
                        jmsDestinations = new HashMap<>();
                    jmsDestinations.put(jmsDestination.getName(), jmsDestination);
                    destinations.put(destination, jmsDestination);
                } else {
                    Destination jmsDestination = destinations.get(destination);
                    if (entry.getKey().endsWith("selector")) {
                        jmsDestination.setSelector(entry.getValue().equalsIgnoreCase("true"));
                    } else if (entry.getKey().endsWith("acknowledge")) {
                        jmsDestination.setAcknowledge(Nums.toInt(entry.getValue(), Session.AUTO_ACKNOWLEDGE));
                    } else if (entry.getKey().endsWith("deliveryMode")) {
                        jmsDestination.setAcknowledge(Nums.toInt(entry.getValue(), DeliveryMode.NON_PERSISTENT));
                    } else if (entry.getKey().endsWith("disableMessageID")) {
                        jmsDestination.setDisableMessageID(entry.getValue().equalsIgnoreCase("true"));
                    } else if (entry.getKey().endsWith("disableMessageTimestamp")) {
                        jmsDestination.setDisableMessageTimestamp(entry.getValue().equalsIgnoreCase("true"));
                    } else if (entry.getKey().endsWith("timeToLive")) {
                        jmsDestination.setTimeToLive(Nums.toLong(entry.getValue(), 0));
                    } else if (entry.getKey().endsWith("priority")) {
                        jmsDestination.setPriority(Nums.toInt(entry.getValue(), 0));
                    }
                }//else
            }//if
        }//for
        for (Destination dest : destinations.values())
            new JmsMessageConsumer(connectionFactory, dest, applicationListener);
        destinations.clear();
    }

    /**
     * 通过名称获取消息信息
     *
     * @param name
     * @return
     */
    public Destination getJmsDestination(String name) {
        if (jmsDestinations == null || jmsDestinations.isEmpty())
            return null;
        return jmsDestinations.get(name);
    }

    /**
     * @param resources
     */
    private void scan(String base, List<GResource> resources) throws Exception {
        List<String> classFiles = new ArrayList<String>();
        for (GResource r : resources) {
            if (r.getName().endsWith(".class")) {// class
                classFiles.add(r.getName());
            } else if (r.getName().endsWith(".jar")) {// jar
                JarFile jf = null;
                try {
                    jf = new JarFile(base + r.getName());
                    for (Enumeration<JarEntry> e = jf.entries(); e
                            .hasMoreElements(); ) {
                        JarEntry entry = e.nextElement();
                        String name = entry.getName();
                        if (name.endsWith(".class"))
                            classFiles.add(name);
                    }
                } catch (IOException ex) {
                    throw new EasyPlatformRuntimeException("can't load jar file "
                            + r.getName(), ex);
                } finally {
                    if (jf != null) {
                        try {
                            jf.close();
                        } catch (Throwable e) {
                        }
                        jf = null;
                    }
                }
            }
        }
        for (String clazz : classFiles) {
            try {
                Class<?> cls = Lang.loadClass(clazz);
                Service service = cls.getAnnotation(Service.class);
                if (service != null && service.type() == ServiceType.CLIENT)
                    serviceFactory.registry(service.value(), cls);
            } catch (EasyPlatformRuntimeException e) {
                throw e;
            } catch (Throwable e) {
            }
        }
        classFiles = null;
    }

    /**
     * 停止服务
     */
    public void stop() {
        if (serviceFactory != null)
            serviceFactory.destory();
        if (applicationListener != null)
            applicationListener.close();
        if (messageEventService != null)
            messageEventService.stop();
        if (webAppListeners != null) {
            try {
                for (WebAppCleanup cleanup : webAppListeners)
                    cleanup.cleanup(webApp);
            } catch (Exception e) {
            }
        }
        if (EngineFactory.me() != null)
            EngineFactory.me().stop();
    }

    /**
     * 返回服务工厂
     *
     * @return
     */
    public ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    /**
     * 获取定义的应用侦听类
     *
     * @return
     */
    public ApplicationListener getApplicationListener() {
        return applicationListener;
    }

    /**
     * Jms连接池
     *
     * @return
     */
    public ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    /**
     * @return
     */
    public boolean isSessionKeepAlive() {
        return webApp.getConfiguration().isTimerKeepAlive();
    }

    /**
     * #客户端轮询间隔，单位秒，默认30000，即30秒
     *
     * @return
     */
    public int getSessionInterval() {
        return sessionInterval;
    }

    /**
     * 工作空间
     *
     * @return
     */
    public String getWorkspace() {
        return workspace;
    }

    /**
     * 获取模板路径
     *
     * @return
     */
    public String getTemplatePath() {
        return templatePath;
    }

    /**
     * 公钥
     *
     * @return
     */
    public PublicKey getPublicKey() {
        return publicKey;
    }

    /**
     * 私钥
     *
     * @return
     */
    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    /**
     * 获取单点登陆配置项
     *
     * @param name
     * @return
     */
    public String getSsoOption(String name) {
        Ini.Section ssoConfig = config.getSection("sso");
        if (ssoConfig == null)
            return null;
        return ssoConfig.get(name);
    }

    /**
     * 获取rpc配置项
     *
     * @param name
     * @return
     */
    public String getRpcOption(String name) {
        Ini.Section rpcConfig = config.getSection("rpc");
        if (rpcConfig == null)
            return null;
        return rpcConfig.get(name);
    }

    /**
     * 订阅消息
     *
     * @param desktop
     */
    public void subscribe(Desktop desktop) {
        MessageObserver observer = new MessageObserver(desktop);
        desktop.addListener((DesktopCleanup) desktop1 -> {
            messageEventHandler.deleteObserver(observer);
            desktop.enableServerPush(false);
            if (LOG.isDebugEnabled())
                LOG.debug("deleteObserver {}:{}", messageEventHandler.countObservers(), desktop.getId());
        });
        messageEventHandler.addObserver(observer);
        if (LOG.isDebugEnabled())
            LOG.debug("addObserver {}:{}", messageEventHandler.countObservers(), desktop.getId());
    }

    /**
     * 分发消息
     *
     * @param projectId
     * @param target
     * @param event
     */
    public void publish(String projectId, String[] target, Event event) {
        if (messageEventService != null)
            messageEventService.onEvent(projectId, target, event);
    }

    @Override
    public Context set(String name, Object value) {
        webApp.setAttribute(name, value);
        return this;
    }

    @Override
    public Set<String> keys() {
        return webApp.getAttributes().keySet();
    }

    @Override
    public boolean has(String key) {
        return webApp.hasAttribute(key);
    }

    @Override
    public Object remove(String name) {
        return webApp.removeAttribute(name);
    }

    @Override
    public Context clear() {
        return this;
    }

    @Override
    public int size() {
        return webApp.getAttributes().size();
    }

    @Override
    public Object get(String name) {
        return webApp.getAttribute(name);
    }

}
