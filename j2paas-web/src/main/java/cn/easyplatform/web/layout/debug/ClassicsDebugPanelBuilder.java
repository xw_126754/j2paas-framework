/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.debug;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.LayoutRegion;
import org.zkoss.zul.Window;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ClassicsDebugPanelBuilder extends AbstractDebugPanelBuilder {

    private Component container;

    private Component panel;

    @Override
    public Component build(Component target) {
        container = target.getFellowIfAny("gv5debugpanel");
        if (container == null) {
            panel = super.build(target);
            target.getFellow("gv5debugclose").detach();
            Window win = new Window(Labels.getLabel("app.debug"), "normal", true);
            win.setPosition("bottom,right");
            win.setSizable(true);
            win.setMaximizable(true);
            win.setTopmost();
            win.setMinimizable(true);
            win.setMinheight(30);
            win.setMinwidth(200);
            win.setWidth("60%");
            win.setHeight("30%");
            win.doOverlapped();
            win.setPage(target.getPage());
            win.appendChild(panel);
            win.addEventListener(Events.ON_CLOSE, this);
            this.container = win;
            return win;
        } else {
            if (container.isVisible())
                return null;
            container.getChildren().clear();
            panel = super.build(target);
            if (container instanceof LayoutRegion) {
                LayoutRegion region = (LayoutRegion) container;
                region.setSplittable(true);
                region.setAutoscroll(true);
            }
            container.appendChild(panel);
            container.setVisible(true);
            return panel;
        }
    }

    @Override
    protected void close() {
        super.close();
        panel.detach();
        panel = null;
        container.setVisible(false);
    }

}
