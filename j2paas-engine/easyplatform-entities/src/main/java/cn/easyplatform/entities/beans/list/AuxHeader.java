/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.list;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = { "title", "description", "style", "rowspan", "colspan",
		"align", "valign", "iconSclass", "image", "hoverimg" })
@XmlAccessorType(XmlAccessType.NONE)
public class AuxHeader implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4385175558822909666L;

	@XmlElement
	private String title;

	@XmlElement
	private String description;

	@XmlElement
	private String style;

	@XmlElement
	private int rowspan = 1;

	@XmlElement
	private int colspan = 1;

	@XmlElement
	private String align = "center";

	@XmlElement
	private String valign;

	@XmlElement
	private String iconSclass;

	@XmlElement
	private String image;

	@XmlElement
	private String hoverimg;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public int getRowspan() {
		return rowspan;
	}

	public void setRowspan(int rowspan) {
		this.rowspan = rowspan <= 0 ? 1 : rowspan;
	}

	public int getColspan() {
		return colspan;
	}

	public void setColspan(int colspan) {
		this.colspan = colspan <= 0 ? 1 : colspan;
	}

	public String getAlign() {
		return align;
	}

	public void setAlign(String align) {
		this.align = align;
	}

	public String getValign() {
		return valign;
	}

	public void setValign(String valign) {
		this.valign = valign;
	}

	public String getIconSclass() {
		return iconSclass;
	}

	public void setIconSclass(String iconSclass) {
		this.iconSclass = iconSclass;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getHoverimg() {
		return hoverimg;
	}

	public void setHoverimg(String hoverimg) {
		this.hoverimg = hoverimg;
	}

	@Override
	public String toString() {
		return title;
	}

}
