/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.api;

import cn.easyplatform.entities.beans.task.Variable;
import cn.easyplatform.entities.helper.FieldTypeAdapter;
import cn.easyplatform.entities.helper.ScopeTypeAdapter;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.ScopeType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"name", "type", "scope", "shareModel", "length",
        "decimal", "acc", "desp", "value", "required"})
@XmlAccessorType(XmlAccessType.NONE)
public class Input implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3319461764194652162L;

    /**
     * 变量名称
     */
    @XmlElement(required = true)
    private String name;

    /**
     * 变量类型
     */
    @XmlJavaTypeAdapter(value = FieldTypeAdapter.class)
    @XmlElement(required = true)
    private FieldType type;

    /**
     * 变量的范围： private:只在当前工作区有效 protected:只在当前工作区有效，但在当前工作区打开的功能以及列表记录可以使用
     * public:在所有的范围内有效
     */
    @XmlJavaTypeAdapter(value = ScopeTypeAdapter.class)
    @XmlElement
    private ScopeType scope = ScopeType.PUBLIC;

    /**
     * 当公共变量传递给其它工作区时是传值还是引用，传值表示获得变量的拷贝 获得后的变量值的改变并不影响原来旧的变量，而引用表示共享内存地址，改变获得
     * 的变量值同时也改变旧的变量值 值可以是{0|1} 默认值为0，表示引用,1表示传值
     */
    @XmlElement
    private int shareModel = Variable.REF_VALUE;

    /**
     * 长度
     */
    @XmlElement
    private int length;

    /**
     * 小数位
     */
    @XmlElement
    private int decimal;

    /**
     * 关联的小数位栏位
     */
    @XmlElement
    private String acc;

    /**
     * 描述
     */
    @XmlElement
    private String desp;

    /**
     * 默认值
     */
    @XmlElement
    private String value;

    /**
     * 是否必须存在
     */
    @XmlElement
    private boolean required;

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public int getShareModel() {
        return shareModel;
    }

    public void setShareModel(int shareModel) {
        this.shareModel = shareModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ScopeType getScope() {
        return scope;
    }

    public void setScope(ScopeType scope) {
        this.scope = scope;
    }

    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getDecimal() {
        return decimal;
    }

    public void setDecimal(int decimal) {
        this.decimal = decimal;
    }

    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

    @Override
    public String toString() {
        return name + "->" + type;
    }

}
