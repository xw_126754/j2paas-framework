/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ApplicationService {

    /**
     * web初始化时需要获取配置信息
     *
     * @param req
     * @return
     */
    IResponseMessage<?> init(SimpleRequestMessage req);

    /**
     * 发送心跳
     *
     * @param req
     * @return
     */
    IResponseMessage<?> ttl(SimpleRequestMessage req);

    /**
     * 获取服务运行时信息
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getRuntime(SimpleRequestMessage req);

    /**
     * 清除会话数据
     *
     * @param req
     * @return
     */
    IResponseMessage<?> purge(SimpleRequestMessage req);

    /**
     * 是否启动log debug输出
     *
     * @param req
     * @return
     */
    IResponseMessage<?> debug(DebugRequestMessage req);

    /**
     * 执行表达式
     *
     * @param req
     * @return
     */
    IResponseMessage<?> executeExpression(ExpressionRequestMessage req);

    /**
     * 切换组织机构
     *
     * @param req
     * @return
     */
    IResponseMessage<?> switchOrg(SwitchOrgRequestMessage req);

    /**
     * 获取多国语言
     *
     * @param req
     * @return
     */
    IResponseMessage<?> i18n(SimpleTextRequestMessage req);

    /**
     * 获取币别小位数
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getAcc(GetAccRequestMessage req);

    /**
     * 获取引导数据
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getWizard(SimpleTextRequestMessage req);

    /**
     * 获取指定项目的
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getConfig(SimpleTextRequestMessage req);

}
