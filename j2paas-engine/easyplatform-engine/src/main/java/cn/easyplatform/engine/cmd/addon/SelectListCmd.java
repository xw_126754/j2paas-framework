/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.addon;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dao.Page;
import cn.easyplatform.dos.Record;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.SqlVo;
import cn.easyplatform.support.sql.SqlParser;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.Collections;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SelectListCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public SelectListCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        SqlVo vo = (SqlVo) req.getBody();
        if (Strings.isBlank(vo.getQuery()))
            return new SimpleResponseMessage(Collections.emptyList());
        if (vo.getParameters() == null && cc.getWorkflowContext() != null) {
            RecordContext rc = null;
            if (vo.getId() != null) {
                ListContext lc = cc.getWorkflowContext().getList(vo.getId());
                if (lc == null)
                    return MessageUtils.dataListNotFound(vo.getId());
                rc = lc.getRecord(vo.getKeys());
                if (rc == null) {
                    Record record = DataListUtils.getRecord(cc, lc, vo.getKeys());
                    rc = lc.createRecord(vo.getKeys(), record);
                    if (lc.getType().equals(Constants.CATALOG)) {
                        rc.setParameter("814", 'R');
                        rc.setParameter("815", Boolean.FALSE);
                        lc.appendRecord(rc);
                    }
                }
            } else
                rc = cc.getWorkflowContext().getRecord();
            SqlParser<Object> sp = RuntimeUtils.createSqlParser(Object.class);
            vo.setQuery(sp.parse(vo.getQuery(), rc));
            vo.setParameters(sp.getParams());
        }
        BizDao dao = cc.getBizDao(vo.getRid());
        if (vo.getPageNo() >= 0 && vo.getPageSize() > 0) {
            Page page = new Page(vo.getPageNo(), vo.getPageSize(), vo.isGetTotalCount());
            page.setOrderBy(vo.getOrderBy());
            Object result = null;
            if (vo.isGetFieldName()) {
                if (vo.getParameters() == null)
                    result = dao.selectMapList(vo.getQuery(), page);
                else
                    result = dao.selectMapList(vo.getQuery(), page, vo.getParameters().toArray());
            } else {
                if (vo.getParameters() == null)
                    result = dao.selectList(vo.getQuery(), page);
                else
                    result = dao.selectList(vo.getQuery(), page, vo.getParameters().toArray());
            }
            if (vo.isGetTotalCount())
                return new SimpleResponseMessage(new Object[]{page.getTotalCount(), result});
            return new SimpleResponseMessage(result);
        } else {
            if (vo.isGetFieldName()) {
                if (vo.getParameters() == null)
                    return new SimpleResponseMessage(dao.selectMapList(vo.getQuery(), null));
                return new SimpleResponseMessage(dao.selectMapList(vo.getQuery(), null, vo.getParameters().toArray()));
            } else {
                if (vo.getParameters() == null)
                    return new SimpleResponseMessage(dao.selectList(vo.getQuery(), null));
                return new SimpleResponseMessage(dao.selectList(vo.getQuery(), null, vo.getParameters().toArray()));
            }
        }
    }

}
