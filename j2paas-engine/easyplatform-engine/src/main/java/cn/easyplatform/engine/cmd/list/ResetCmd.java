/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.list;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.ListResetRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.datalist.ListResetVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ResetCmd extends AbstractCommand<ListResetRequestMessage> {

	/**
	 * @param req
	 */
	public ResetCmd(ListResetRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		WorkflowContext ctx = cc.getWorkflowContext();
		ListResetVo lv = req.getBody();
		ListContext lc = ctx.getList(lv.getId());
		if (lc == null)
			return MessageUtils.dataListNotFound(req.getBody().getId());
		if (lv.getFromKeys() != null) {
			lc.setHostKey(req.getBody().getFromKeys());
			lc.clear(1);// 清除所属的数据
		} else
			lc.clear(2);// 清除所有的数据
		return new SimpleResponseMessage();
	}

	@Override
	public String getName() {
		return "list.Reset";
	}
}
