/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.vfs;

import cn.easyplatform.i18n.I18N;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleTextRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.type.IResponseMessage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MkdirCmd extends AbstractCommand<SimpleTextRequestMessage> {

    private static Logger log = LoggerFactory.getLogger(MkdirCmd.class);

    /**
     * @param req
     */
    public MkdirCmd(SimpleTextRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        String fn = req.getBody();
        if (fn.startsWith("$7")) {
            fn = cc.getRealPath(fn);
            File file = null;
            try {
                file = new File(fn);
                if (file.exists())
                    return new SimpleResponseMessage("e026", I18N.getLabel("vfs.exists", FilenameUtils.getName(fn)));
                FileUtils.forceMkdir(file);
            } catch (Exception e) {
                log.error("vfs write", e);
                return new SimpleResponseMessage("e027", I18N.getLabel("vfs.access.error", FilenameUtils.getName(fn)));
            }
        } else {
            File file = new File(fn);
            if (file.exists())
                return new SimpleResponseMessage("e026", I18N.getLabel("vfs.exists", FilenameUtils.getName(fn)));
            try {
                FileUtils.forceMkdir(file);
            } catch (IOException e) {
                log.error("vfs write", e);
                return new SimpleResponseMessage("e027", I18N.getLabel("vfs.access.error", FilenameUtils.getName(fn)));
            }
        }
        return new SimpleResponseMessage();
    }

}
