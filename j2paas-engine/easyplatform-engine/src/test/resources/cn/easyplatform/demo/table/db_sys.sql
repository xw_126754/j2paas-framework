#create procedure sp_page @qry varchar(16384), @ipage int, @num int
#as
#begin
# declare @rcount int
# declare @execsql varchar(16384)
# select @rcount=@ipage*@num
# set @execsql='set rowcount '|| convert(varchar, @rcount)
# set @execsql=@execsql || stuff(@qry, charindex('SELECT ', upper(@qry)), 6, ' SELECT sybid=identity(12), ')
# set @execsql=stuff(@execsql, charindex(' FROM ', upper(@execsql)), 6, ' INTO #temptable FROM ')
# set @execsql=@execsql || ' SELECT * FROM #temptable WHERE sybid >' || convert(varchar, (@ipage-1)*@num)
# set @execsql=@execsql || ' AND sybid<=' || convert(varchar, @ipage*@num) + ' set rowcount 0'
# execute(@execsql)
#end

#用户表
insert into sys_user_info(userId,name,type,password,state,sex,phone) values ('admin','admin',3,'jFIJIMUI6N4A4W65S83EOCh8GD364B92Dnl6Kcg0+jk=',1,'male','13806053570')
insert into sys_user_info(userId,name,type,password,state,sex,phone) values ('david','littleDog',0,'jFIJIMUI6N4A4W65S83EOCh8GD364B92Dnl6Kcg0+jk=',1,'male','13806053570')
insert into sys_user_info(userId,name,type,password,state,sex,phone) values ('lxr','蓝晓茹',1,'jFIJIMUI6N4A4W65S83EOCh8GD364B92Dnl6Kcg0+jk=',1,'female','87220442')
insert into sys_user_info(userId,name,type,password,state,sex,phone) values ('daniel','肖移海',2,'jFIJIMUI6N4A4W65S83EOCh8GD364B92Dnl6Kcg0+jk=',1,'female','87220442')

#代理表
insert into sys_agent_info(id,operatorId,agentId,sdate,edate,state) values (1,'daniel','lxr','20150331','20150408',1)

#角色表
insert into sys_role_info(roleId,name) values ('Admin','超级用户')
insert into sys_role_info(roleId,name) values ('Manager','主管')
insert into sys_role_info(roleId,name) values ('Employee','员工')
insert into sys_role_info(roleId,name) values ('Mobile','移动用户')

#组
insert into sys_group_info(groupid,name) values (1,'开发组')
insert into sys_group_info(groupid,name) values (2,'测试组')
insert into sys_group_info(groupid,name) values (3,'文档组')

#用户组
insert into sys_user_group_info(groupid,userId) values (1,'daniel')
insert into sys_user_group_info(groupid,userId) values (1,'david')
insert into sys_user_group_info(groupid,userId) values (2,'daniel')
insert into sys_user_group_info(groupid,userId) values (2,'lxr')
insert into sys_user_group_info(groupid,userId) values (3,'lxr')
insert into sys_user_group_info(groupid,userId) values (3,'david')

#机构表
insert into sys_org_info(orgId,name,address,manager,phone) values ('RJ001','融建信息技术(厦门)有限公司','软件园观日路32号303单元30306','david','13806053570')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ002','融建深圳分公司','RJ001','罗湖区xxxx','daniel','8989898')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ003','融建北京分公司','RJ001','海淀区xxxx','简','8989898')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ004','融建上海分公司','RJ001','浦东新区xxxx','陈','8989898')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ005','华中区','RJ001','武汉江汉区','肖建喜','8989898')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ006','武汉办事处','RJ005','武汉新洲','陈','87773887')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ007','长沙办事处','RJ005','长江江陵区','江','8265678')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ008','常德办事处','RJ007','湖南常德长安区','阮','896433224')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ009','珠海办事处','RJ002','珠海MX办公大楼','黄','8989124')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ010','广州办事处','RJ002','花都锦江大酒店','管','2369898')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ011','石家庄办事处','RJ003','石家庄长安','花','4700888')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ012','融建重庆分公司','RJ001','重庆江北区','陈','7889898')
insert into sys_org_info(orgId,name,parent,address,manager,phone) values ('RJ013','杭州办事处','RJ004','杭州西湖','习','8009898')
#用户机构表
insert into sys_user_org_info(userId,orgId) values('david','RJ001')
insert into sys_user_org_info(userId,orgId) values('david','RJ002')
insert into sys_user_org_info(userId,orgId) values('david','RJ003')
insert into sys_user_org_info(userId,orgId) values('david','RJ004')
insert into sys_user_org_info(userId,orgId) values('daniel','RJ001')
insert into sys_user_org_info(userId,orgId) values('lxr','RJ004')
insert into sys_user_org_info(userId,orgId) values('admin','RJ004')

#机构角色表
insert into sys_org_role_info(orgId,roleId) values('RJ001','Manager')
insert into sys_org_role_info(orgId,roleId) values('RJ001','Employee')
insert into sys_org_role_info(orgId,roleId) values('RJ004','Employee')

#用户角色表
insert into sys_user_role_info(userId,roleId,deviceType,orderNo) values('david','Admin','ajax',1)
#insert into sys_user_role_info(userId,roleId,deviceType,orderNo) values('david','Manager','ajax',2)
insert into sys_user_role_info(userId,roleId,deviceType,orderNo) values('david','Employee','ajax',3)
insert into sys_user_role_info(userId,roleId,deviceType,orderNo) values('daniel','Manager','ajax',1)
insert into sys_user_role_info(userId,roleId,deviceType,orderNo) values('lxr','Employee','ajax',1)
insert into sys_user_role_info(userId,roleId,deviceType,orderNo) values('lxr','Mobile','mil',1)

#菜单表
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('00001','基础控件',1,'','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('00002','扩展控件',2,'','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('00003','页面处理',3,'','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('00004','工作流',4,'','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('00005','批量处理',5,'','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('00006','外部接口',6,'','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('00007','其它',7,'','')
#第二级菜单
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000010','显示控件',0,'00001','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000011','输入控件',1,'00001','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000012','布局控件',2,'00001','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000013','列表',3,'00001','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000014','树',4,'00001','')

insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000020','图表和报表',1,'00002','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000021','多选列表',2,'00002','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000022','WebOffice',3,'00002','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000023','下拉列表',4,'00002','')

insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000030','普通页面',1,'00003','demo.task.customer,demo.task.goods,demo.task.page.load,demo.task.complex.1,demo.task.component.1,demo.task.component.2,demo.task.component.3')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000031','普通列表',2,'00003','demo.task.goods.d1,demo.task.goods.d2,demo.task.goods.d3,demo.task.goods.d4,demo.task.goods.d5,demo.task.list.batch')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000032','树状列表',3,'00003','demo.task.tree.1,demo.task.tree.2,demo.task.tree.3,demo.task.tree.4,demo.task.tree.5,demo.task.tree.6')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000033','报表列表',4,'00003','demo.task.group.1')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000034','页面与列表',5,'00003','demo.task.goods.d6,demo.task.customer.d1,demo.task.customer.d3,demo.task.action.1,demo.task.datalist.load')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000035','报表和图表',6,'00003','demo.task.pie3d.1,demo.task.pie3d.2,demo.task.crosstab.1,demo.task.subreport.1,demo.task.report.1,demo.task.report.2,demo.task.custom.report.1,demo.task.charts.1,demo.task.charts.2,demo.task.charts.3')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000036','事务控制',7,'00003','')

insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000050','功能测试',1,'00005','demo.task.batch.1,demo.task.batch.2,demo.task.batch.3,demo.task.batch.4,demo.task.batch.5')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000051','特殊测试',2,'00005','demo.task.batch.6,demo.task.batch.7')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000052','工作树测试',3,'00005','')

insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000060','调用指定的类',1,'00006','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000061','调用WEB服务',2,'00006','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000062','调用指定脚本',3,'00006','')

insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000063','上传',1,'00007','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000064','下载',2,'00007','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000065','导入',3,'00007','')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000066','后台处理',4,'00007','')

insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000100','测试案例',1,'00004','demo.task.bpm.user.leave,demo.task.bpm.actorall,demo.task.bpm.forkjoin,demo.task.bpm.subprocess')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000101','任务管理',2,'00004','demo.task.wf_task1,demo.task.wf_task2,demo.task.wf_task3')
insert into sys_menu_info(menuId,name,orderNo,parentMenuId,tasks) values ('000102','流程管理',3,'00004','demo.task.bpm.order1')

#角色菜单表
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Admin','00003',1)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Admin','00002',3)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Admin','00001',4)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Admin','00004',5)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Admin','00005',6)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Admin','00006',7)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Admin','00007',8)

insert into sys_role_menu_info(roleId,menuId,orderNo) values('Manager','00003',1)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Manager','00004',2)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Manager','00002',3)

insert into sys_role_menu_info(roleId,menuId,orderNo) values('Employee','00003',1)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Employee','00004',2)

insert into sys_role_menu_info(roleId,menuId,orderNo) values('Mobile','00003',1)
insert into sys_role_menu_info(roleId,menuId,orderNo) values('Mobile','00004',2)

#i18N以及应用信息表
insert into sys_message_info(code,zh_CN,zh_TW,en_US,status) values('i000','{0}','{0}','{0}','C')
insert into sys_message_info(code,zh_CN,zh_TW,en_US,status) values('c012','简体初始提示{0}','繁体初始提示{0}','英文初始提示{0}','C')
insert into sys_message_info(code,zh_CN,zh_TW,en_US,status) values('c013','简体刷新提示','繁体刷新提示','英文刷新提示','C')
insert into sys_message_info(code,zh_CN,zh_TW,en_US,status) values('w012','简体确认提示{0}','繁体确认提示{0}','英文确认提示{0}','C')
insert into sys_message_info(code,zh_CN,zh_TW,en_US,status) values('demo.i18n.1','简体','繁体','English','C')