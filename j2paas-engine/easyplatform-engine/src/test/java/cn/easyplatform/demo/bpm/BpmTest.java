/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.bpm;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BpmTest extends AbstractDemoTest {

	public void test1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			log.debug("正在新建demo工作流");
			conn = JdbcTransactions.getConnection(ds1);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.bpm.user.leave");
			pstmt.setString(2, "请假流程");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.BPM.getName());
			pstmt.setString(5, new String(Streams.readBytes(getClass()
					.getResourceAsStream("leave.snaker"))));
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.bpm.forkjoin");
			pstmt.setString(2, "并行测试");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.BPM.getName());
			pstmt.setString(5, new String(Streams.readBytes(getClass()
					.getResourceAsStream("forkjoin.snaker"))));
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.bpm.actorall");
			pstmt.setString(2, "会签测试");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.BPM.getName());
			pstmt.setString(5, new String(Streams.readBytes(getClass()
					.getResourceAsStream("actorall.snaker"))));
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.bpm.subprocess");
			pstmt.setString(2, "子流程测试");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.BPM.getName());
			pstmt.setString(5, new String(Streams.readBytes(getClass()
					.getResourceAsStream("subprocess.snaker"))));
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			log.debug("新建demo工作流成功");
		} catch (Exception ex) {
			throw new EasyPlatformRuntimeException("PageTest", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}
}
