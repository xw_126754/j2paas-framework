/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.eject;

import cn.easyplatform.lang.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

public class EjectByGetter implements Ejecting {

	private static final Logger log = LoggerFactory.getLogger(EjectByGetter.class);

	private Method getter;

	public EjectByGetter(Method getter) {
		this.getter = getter;
	}

	public Object eject(Object obj) {
		try {
			return null == obj ? null : getter.invoke(obj);
		} catch (Exception e) {
			if (log.isInfoEnabled())
				log.info("Fail to value by getter", e);
			throw Lang.makeThrow(
					"Fail to invoke getter %s.'%s()' because [%s]: %s", getter
							.getDeclaringClass().getName(), getter.getName(),
					Lang.unwrapThrow(e), Lang.unwrapThrow(e).getMessage());
		}
	}

}
