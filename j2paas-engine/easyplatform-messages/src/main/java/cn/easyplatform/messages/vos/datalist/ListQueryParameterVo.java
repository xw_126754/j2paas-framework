/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListQueryParameterVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	private List<Object> values;
	
	private String op;

	private String rp;

	public ListQueryParameterVo(String name,String op, String rp) {
		this.name = name;
		this.op = op;
		this.rp = rp;
	}

	public String getName() {
		return name;
	}

	public String getOp() {
		return op;
	}

	public String getRp() {
		return rp;
	}

	public Collection<Object> getValues() {
		return values;
	}

	public void setValue(Object value) {
		if (values == null)
			values = new ArrayList<Object>();
		values.add(value);
	}
}
