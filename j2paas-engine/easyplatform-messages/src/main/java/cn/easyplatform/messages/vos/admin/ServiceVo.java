/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.admin;

import java.io.Serializable;
import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ServiceVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    private int type;

    private int state;

    private Map<String, Object> data;

    /**
     * @param id
     * @param name
     * @param type
     */
    public ServiceVo(String id, String name, int type, int state) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.state = state;
    }

    public ServiceVo(String id, int type, int state) {
        this.id = id;
        this.type = type;
        this.state = state;
    }

    public ServiceVo(String id, int type) {
        this.id = id;
        this.type = type;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @return the state
     */
    public int getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(int state) {
        this.state = state;
    }

    public void setValue(String key, Object value) {
        if (data == null)
            data = new HashMap<>();
        data.put(key, value);
    }

    public void setRuntimeInfo(Map<String, Object> data) {
        this.data = data;
    }

    public Object getValue(String key) {
        if (data == null)
            return null;
        return data.get(key);
    }

    public String getAsString(String key) {
        if (data == null)
            return null;
        return (String) data.get(key);
    }

    public Date getAsDate(String key) {
        if (data == null)
            return null;
        return (Date) data.get(key);
    }

    public Set<String> getKeySet() {
        if (data == null)
            return null;
        return data.keySet();
    }
}
