/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.services;

import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.spi.extension.ApplicationService;

import javax.sql.DataSource;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/4/24 15:34
 * @Modified By:
 */
public interface IDataSource extends DataSource, ApplicationService {

    /**
     * 获取对应的连接参数
     *
     * @Return cn.easyplatform.entities.beans.ResourceBean
     **/
    ResourceBean getEntity();

    /**
     * 重置sql状态
     */
    void resetSqlStat(Long id);

    /**
     * 重置数据源状态
     */
    void resetDataSourceStat();
}
