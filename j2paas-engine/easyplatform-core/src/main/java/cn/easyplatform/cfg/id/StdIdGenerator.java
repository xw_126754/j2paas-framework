/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.cfg.id;

import cn.easyplatform.cfg.IdGenerator;
import cn.easyplatform.dao.SeqDao;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StdIdGenerator implements IdGenerator {

    private SeqDao dao;

    public StdIdGenerator(SeqDao dao) {
        this.dao = dao;
    }

    @Override
    public Long getNextId(String name) {
        return dao.nextVal(name);
    }

    @Override
    public void delete(String name) {
        dao.delete(name);
    }

    @Override
    public void reset(String name, long value) {
        dao.reset(name, value);
    }

    @Override
    public List<Object[]> getList() {
        return dao.getList();
    }
}
