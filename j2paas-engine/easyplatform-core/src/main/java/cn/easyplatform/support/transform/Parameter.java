/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.transform;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Parameter {

    private String[] sheets;
    private int columnsRow;
    private boolean progress;
    private int countOfThread;

    public String[] getSheets() {
        return sheets;
    }

    public void setSheets(String[] sheets) {
        this.sheets = sheets;
    }

    public int getColumnsRow() {
        return columnsRow;
    }

    public void setColumnsRow(int columnsRow) {
        this.columnsRow = columnsRow;
    }

    public boolean isProgress() {
        return progress;
    }

    public void setProgress(boolean progress) {
        this.progress = progress;
    }

    public int getCountOfThread() {
        return countOfThread;
    }

    public void setCountOfThread(int countOfThread) {
        this.countOfThread = countOfThread;
    }
}
