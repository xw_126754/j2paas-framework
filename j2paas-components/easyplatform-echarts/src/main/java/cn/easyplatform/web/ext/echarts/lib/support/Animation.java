/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.support;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Animation implements Serializable {
    /**
     * 是否启用图表初始化动画，默认开启，建议IE8-关闭，（详见 animation，相关的还有 addDataAnimation， animationThreshold， animationDuration， animationEasing）
     */
    private Boolean animation;
    /**
     * 动画元素阀值，产生的图形原素超过2000不出动画，默认开启，建议IE8-关闭
     */
    private Integer animationThreshold;
    /**
     * 进入动画时长，单位ms
     */
    private Integer animationDuration;
    /**
     * 主元素的缓动效果
     */
    private Object animationEasing;
    /**
     * 初始动画的延迟，支持回调函数，可以通过每个数据返回不同的 delay 时间实现更戏剧的初始动画效果
     */
    private Object animationDelay;
    /**
     * 数据更新动画的时长
     */
    private Integer animationDurationUpdate;
    /**
     * 数据更新动画的缓动效果
     */
    private Object animationEasingUpdate;
    /**
     * 数据更新动画的延迟，支持回调函数，可以通过每个数据返回不同的 delay 时间实现更戏剧的更新动画效果
     */
    private Object animationDelayUpdate;

    public Boolean animation() {
        return animation;
    }

    public Animation animation(Boolean animation) {
        this.animation = animation;
        return this;
    }

    public Integer animationThreshold() {
        return animationThreshold;
    }

    public Animation animationThreshold(Integer animationThreshold) {
        this.animationThreshold = animationThreshold;
        return this;
    }

    public Integer animationDuration() {
        return animationDuration;
    }

    public Animation animationDuration(Integer animationDuration) {
        this.animationDuration = animationDuration;
        return this;
    }

    public Object animationEasing() {
        return animationEasing;
    }

    public Animation animationEasing(Object animationEasing) {
        this.animationEasing = animationEasing;
        return this;
    }

    public Object animationDelay() {
        return animationDelay;
    }

    public Animation animationDelay(Object animationDelay) {
        this.animationDelay = animationDelay;
        return this;
    }

    public Integer animationDurationUpdate() {
        return animationDurationUpdate;
    }

    public Animation animationDurationUpdate(Integer animationDurationUpdate) {
        this.animationDurationUpdate = animationDurationUpdate;
        return this;
    }

    public Object animationEasingUpdate() {
        return animationEasingUpdate;
    }

    public Animation animationEasingUpdate(Object animationEasingUpdate) {
        this.animationEasingUpdate = animationEasingUpdate;
        return this;
    }

    public Object animationDelayUpdate() {
        return animationDelayUpdate;
    }

    public Animation animationDelayUpdate(Object animationDelayUpdate) {
        this.animationDelayUpdate = animationDelayUpdate;
        return this;
    }

    public Boolean getAnimation() {
        return animation;
    }

    public void setAnimation(Boolean animation) {
        this.animation = animation;
    }

    public Integer getAnimationThreshold() {
        return animationThreshold;
    }

    public void setAnimationThreshold(Integer animationThreshold) {
        this.animationThreshold = animationThreshold;
    }

    public Integer getAnimationDuration() {
        return animationDuration;
    }

    public void setAnimationDuration(Integer animationDuration) {
        this.animationDuration = animationDuration;
    }

    public Object getAnimationEasing() {
        return animationEasing;
    }

    public void setAnimationEasing(Object animationEasing) {
        this.animationEasing = animationEasing;
    }

    public Object getAnimationDelay() {
        return animationDelay;
    }

    public void setAnimationDelay(Object animationDelay) {
        this.animationDelay = animationDelay;
    }

    public Integer getAnimationDurationUpdate() {
        return animationDurationUpdate;
    }

    public void setAnimationDurationUpdate(Integer animationDurationUpdate) {
        this.animationDurationUpdate = animationDurationUpdate;
    }

    public Object getAnimationEasingUpdate() {
        return animationEasingUpdate;
    }

    public void setAnimationEasingUpdate(Object animationEasingUpdate) {
        this.animationEasingUpdate = animationEasingUpdate;
    }

    public Object getAnimationDelayUpdate() {
        return animationDelayUpdate;
    }

    public void setAnimationDelayUpdate(Object animationDelayUpdate) {
        this.animationDelayUpdate = animationDelayUpdate;
    }
}
