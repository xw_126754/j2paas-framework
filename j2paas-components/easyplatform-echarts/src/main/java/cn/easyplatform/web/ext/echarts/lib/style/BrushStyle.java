/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BrushStyle {

    private Integer borderWidth;

    private Object color;

    private Object borderColor;

    private Object width;

    public Object color() {
        return color;
    }

    public BrushStyle color(Object color) {
        this.color = color;
        return this;
    }

    public Object borderColor() {
        return borderColor;
    }

    public BrushStyle borderColor(Object borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Object width() {
        return width;
    }

    public BrushStyle width(Object width) {
        this.width = width;
        return this;
    }

    public Integer borderWidth() {
        return borderWidth;
    }

    public BrushStyle borderWidth(Integer borderColor) {
        this.borderWidth = borderColor;
        return this;
    }

}
