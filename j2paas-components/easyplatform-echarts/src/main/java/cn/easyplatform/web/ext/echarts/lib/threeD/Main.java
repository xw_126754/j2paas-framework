/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import java.io.Serializable;

public class Main implements Serializable {
    /**
     * 主光源的颜色。
     */
    private String color;
    /**
     * 主光源的强度。
     */
    private int intensity;
    /**
     * 主光源是否投射阴影。默认为关闭。
     *
     * 开启阴影可以给场景带来更真实和有层次的光照效果。但是同时也会增加程序的运行开销。
     */
    private boolean shadow;
    /**
     * 阴影的质量。可选'low', 'medium', 'high', 'ultra'
     */
    private String shadowQuality;
    /**
     * 主光源绕 x 轴，即上下旋转的角度。配合 beta 控制光源的方向。
     */
    private int alpha;
    /**
     * 主光源绕 y 轴，即左右旋转的角度。
     */
    private int beta;

    public String color() {
        return this.color;
    }
    public Main color(String color) {
        this.color = color;
        return this;
    }

    public Integer intensity() {
        return this.intensity;
    }
    public Main intensity(Integer intensity) {
        this.intensity = intensity;
        return this;
    }

    public Boolean shadow() {
        return this.shadow;
    }
    public Main shadow(Boolean shadow) {
        this.shadow = shadow;
        return this;
    }

    public String shadowQuality() {
        return this.shadowQuality;
    }
    public Main shadowQuality(String shadowQuality) {
        this.shadowQuality = shadowQuality;
        return this;
    }

    public Integer alpha() {
        return this.alpha;
    }
    public Main alpha(Integer alpha) {
        this.alpha = alpha;
        return this;
    }

    public Integer beta() {
        return this.beta;
    }
    public Main beta(Integer beta) {
        this.beta = beta;
        return this;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getIntensity() {
        return intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    public boolean isShadow() {
        return shadow;
    }

    public void setShadow(boolean shadow) {
        this.shadow = shadow;
    }

    public String getShadowQuality() {
        return shadowQuality;
    }

    public void setShadowQuality(String shadowQuality) {
        this.shadowQuality = shadowQuality;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public int getBeta() {
        return beta;
    }

    public void setBeta(int beta) {
        this.beta = beta;
    }
}
