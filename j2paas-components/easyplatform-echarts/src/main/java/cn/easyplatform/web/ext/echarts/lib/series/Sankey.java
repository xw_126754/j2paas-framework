/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Sankey extends Series {
    private Object width;
    private Object height;
    private Integer nodeWidth;
    private Integer nodeGap;
    private Integer layoutIterations;
    private LineStyle lineStyle;
    private List<Object> nodes;
    private List<Object> links;
    private List<Object> edges;
    private Object layout;
    /**
     * treemap 组件离容器左侧的距离
     */
    private Object left;
    /**
     * treemap 组件离容器上侧的距离
     */
    private Object top;
    /**
     * treemap 组件离容器右侧的距离
     */
    private Object right;
    /**
     * treemap 组件离容器下侧的距离
     */
    private Object bottom;

    /**
     * 桑基图中节点的对齐方式，默认是双端对齐，可以设置为左对齐或右对齐，对应的值分别是：
     * <p>
     * justify: 节点双端对齐。
     * left: 节点左对齐。
     * right: 节点右对齐。
     */
    private String nodeAlign;
    /**
     * 桑基图中节点的布局方向，可以是水平的从左往右，也可以是垂直的从上往下，对应的参数值分别是 horizontal, vertical。
     */
    private String orient;
    /**
     * 控制节点拖拽的交互，默认开启。开启后，用户可以将图中任意节点拖拽到任意位置。若想关闭此交互，只需将值设为 false 就行了
     */
    private Boolean draggable;
    /**
     * 鼠标 hover 到节点或边上，相邻接的节点和边高亮的交互，默认关闭，可手动开启
     */
    private Object focusNodeAdjacency;

    private Levels levels;

    public Sankey() {
        this.type="sankey";
    }

    public Levels levels() {
        if (levels == null)
            levels = new Levels();
        return levels;
    }

    public Sankey levels(Levels levels) {
        this.levels = levels;
        return this;
    }

    public Object focusNodeAdjacency() {
        return this.focusNodeAdjacency;
    }

    public Sankey focusNodeAdjacency(Object focusNodeAdjacency) {
        this.focusNodeAdjacency = focusNodeAdjacency;
        return this;
    }

    public Boolean draggable() {
        return this.draggable;
    }

    public Sankey draggable(Boolean draggable) {
        this.draggable = draggable;
        return this;
    }

    public String orient() {
        return this.orient;
    }

    public Sankey orient(String orient) {
        this.orient = orient;
        return this;
    }

    public String nodeAlign() {
        return this.nodeAlign;
    }

    public Sankey nodeAlign(String nodeAlign) {
        this.nodeAlign = nodeAlign;
        return this;
    }

    public Object left() {
        return this.left;
    }

    public Sankey left(Object left) {
        this.left = left;
        return this;
    }

    public Object top() {
        return this.top;
    }

    public Sankey top(Object top) {
        this.top = top;
        return this;
    }

    public Object right() {
        return this.right;
    }

    public Sankey right(Object right) {
        this.right = right;
        return this;
    }

    public Object bottom() {
        return this.bottom;
    }

    public Sankey bottom(Object bottom) {
        this.bottom = bottom;
        return this;
    }

    public Object nodeWidth() {
        return nodeWidth;
    }

    public Sankey nodeWidth(Integer nodeWidth) {
        this.nodeWidth = nodeWidth;
        return this;
    }

    public Object layout() {
        return layout;
    }

    public Sankey layout(Object layout) {
        this.layout = layout;
        return this;
    }

    public Object nodeGap() {
        return nodeGap;
    }

    public Sankey nodeGap(Integer nodeGap) {
        this.nodeGap = nodeGap;
        return this;
    }

    public Object layoutIterations() {
        return layoutIterations;
    }

    public Sankey layoutIterations(Integer layoutIterations) {
        this.layoutIterations = layoutIterations;
        return this;
    }

    public Object height() {
        return height;
    }

    public Sankey height(Object height) {
        this.height = height;
        return this;
    }

    public Object width() {
        return width;
    }

    public Sankey width(Object width) {
        this.width = width;
        return this;
    }

    public Object edges() {
        return edges;
    }

    public Sankey edges(Object edge) {
        if (this.edges == null)
            this.edges = new ArrayList<Object>();
        this.edges.add(edge);
        return this;
    }

    public Object links() {
        return links;
    }

    public Sankey links(Object link) {
        if (this.links == null)
            links = new ArrayList<Object>();
        this.links.add(link);
        return this;
    }

    public Object nodes() {
        return nodes;
    }

    public Sankey nodes(Object node) {
        if (this.nodes == null)
            nodes = new ArrayList<Object>();
        this.nodes.add(node);
        return this;
    }

    public LineStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new LineStyle();
        return lineStyle;
    }

    public Sankey lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Integer getNodeWidth() {
        return nodeWidth;
    }

    public void setNodeWidth(Integer nodeWidth) {
        this.nodeWidth = nodeWidth;
    }

    public Integer getNodeGap() {
        return nodeGap;
    }

    public void setNodeGap(Integer nodeGap) {
        this.nodeGap = nodeGap;
    }

    public Integer getLayoutIterations() {
        return layoutIterations;
    }

    public void setLayoutIterations(Integer layoutIterations) {
        this.layoutIterations = layoutIterations;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public List<Object> getNodes() {
        return nodes;
    }

    public void setNodes(List<Object> nodes) {
        this.nodes = nodes;
    }

    public List<Object> getLinks() {
        return links;
    }

    public void setLinks(List<Object> links) {
        this.links = links;
    }

    public List<Object> getEdges() {
        return edges;
    }

    public void setEdges(List<Object> edges) {
        this.edges = edges;
    }

    public Object getLayout() {
        return layout;
    }

    public void setLayout(Object layout) {
        this.layout = layout;
    }

    public Object getLeft() {
        return left;
    }

    public void setLeft(Object left) {
        this.left = left;
    }

    public Object getTop() {
        return top;
    }

    public void setTop(Object top) {
        this.top = top;
    }

    public Object getRight() {
        return right;
    }

    public void setRight(Object right) {
        this.right = right;
    }

    public Object getBottom() {
        return bottom;
    }

    public void setBottom(Object bottom) {
        this.bottom = bottom;
    }

    public String getNodeAlign() {
        return nodeAlign;
    }

    public void setNodeAlign(String nodeAlign) {
        this.nodeAlign = nodeAlign;
    }

    public String getOrient() {
        return orient;
    }

    public void setOrient(String orient) {
        this.orient = orient;
    }

    public Boolean getDraggable() {
        return draggable;
    }

    public void setDraggable(Boolean draggable) {
        this.draggable = draggable;
    }

    public Object getFocusNodeAdjacency() {
        return focusNodeAdjacency;
    }

    public void setFocusNodeAdjacency(Object focusNodeAdjacency) {
        this.focusNodeAdjacency = focusNodeAdjacency;
    }

    public Levels getLevels() {
        return levels;
    }

    public void setLevels(Levels levels) {
        this.levels = levels;
    }

    public static class Levels {
        private Integer depth;
        private ItemStyle itemStyle;
        private LineStyle lineStyle;

        public LineStyle lineStyle() {
            if (lineStyle == null)
                lineStyle = new LineStyle();
            return lineStyle;
        }

        public Levels lineStyle(LineStyle lineStyle) {
            this.lineStyle = lineStyle;
            return this;
        }

        public ItemStyle itemStyle() {
            if (itemStyle == null)
                itemStyle = new ItemStyle();
            return itemStyle;
        }

        public Levels itemStyle(ItemStyle itemStyle) {
            this.itemStyle = itemStyle;
            return this;
        }

        public Integer depth() {
            return depth;
        }

        public Levels depth(Integer depth) {
            this.depth = depth;
            return this;
        }

        public Integer getDepth() {
            return depth;
        }

        public void setDepth(Integer depth) {
            this.depth = depth;
        }

        public ItemStyle getItemStyle() {
            return itemStyle;
        }

        public void setItemStyle(ItemStyle itemStyle) {
            this.itemStyle = itemStyle;
        }

        public LineStyle getLineStyle() {
            return lineStyle;
        }

        public void setLineStyle(LineStyle lineStyle) {
            this.lineStyle = lineStyle;
        }
    }
}
