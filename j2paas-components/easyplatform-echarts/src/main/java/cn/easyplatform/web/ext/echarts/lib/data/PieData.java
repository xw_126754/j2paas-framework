/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import cn.easyplatform.web.ext.echarts.lib.Tooltip;
import cn.easyplatform.web.ext.echarts.lib.series.base.LabelLine;
import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;

import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PieData implements Serializable {

    private static final long serialVersionUID = 7218201600361155091L;

    private String name;
    private Object value;
    private Boolean selected;
    private LabelStyle label;
    private ItemStyle itemStyle;
    private Emphasis emphasis;
    private Tooltip tooltip;
    private LabelLine labelLine;

    public PieData(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public LabelLine labelLine() {
        if (labelLine == null)
            labelLine = new LabelLine();
        return this.labelLine;
    }

    public PieData labelLine(LabelLine labelLine) {
        this.labelLine = labelLine;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return this.label;
    }

    public PieData label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public PieData itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public Tooltip tooltip() {
        if (tooltip == null)
            tooltip = new Tooltip();
        return this.tooltip;
    }

    public PieData tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return this.emphasis;
    }

    public PieData emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public String name() {
        return this.name;
    }

    public PieData name(String name) {
        this.name = name;
        return this;
    }

    public Boolean selected() {
        return this.selected;
    }

    public PieData selected(Boolean selected) {
        this.selected = selected;
        return this;
    }

    public Object value() {
        return this.value;
    }

    public PieData value(Object value) {
        this.value = value;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }

    public LabelLine getLabelLine() {
        return labelLine;
    }

    public void setLabelLine(LabelLine labelLine) {
        this.labelLine = labelLine;
    }
}
