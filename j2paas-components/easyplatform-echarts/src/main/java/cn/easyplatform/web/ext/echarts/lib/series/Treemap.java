/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.series.base.Breadcrumb;
import cn.easyplatform.web.ext.echarts.lib.series.base.Level;
import cn.easyplatform.web.ext.echarts.lib.series.base.Silent;
import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Treemap extends Point {
    private String type = "treemap";
    private String name;
    private Double squareRatio;
    private Object leafDepth;
    private Object roam;
    private String nodeClick;
    private Double zoomToNodeRatio;
    private Level[] levels;
    private Silent silent;
    private Integer visualDimension;
    private Object visualMin;
    private Object visualMax;
    private Object[] colorAlpha;
    private Double colorSaturation;
    private Object colorMappingBy;
    private Double visibleMin;
    private Double childrenVisibleMin;
    private Breadcrumb breadcrumb;
    private List<Object> data;
    private Object animationDuration;
    private Object animationEasing;
    private Object animationDelay;
    private LabelStyle label;
    private ItemStyle itemStyle;
    private Object width;
    private Object height;
    /**
     * 当节点可以下钻时的提示符。只能是字符。
     */
    private String drillDownIcon;
    /**
     * upperLabel 用于显示矩形的父节点的标签
     */
    private LabelStyle upperLabel;
    /**
     *
     */
    private Emphasis emphasis;

    public LabelStyle upperLabel() {
        if (upperLabel == null)
            upperLabel = new LabelStyle();
        return upperLabel;
    }

    public Treemap upperLabel(LabelStyle upperLabel) {
        this.upperLabel = upperLabel;
        return this;
    }

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return emphasis;
    }

    public Treemap emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public String name() {
        return this.name;
    }

    public Treemap name(String name) {
        this.name = name;
        return this;
    }

    public String drillDownIcon() {
        return this.drillDownIcon;
    }

    public Treemap drillDownIcon(String drillDownIcon) {
        this.drillDownIcon = drillDownIcon;
        return this;
    }

    public Object width() {
        return this.width;
    }

    public Treemap width(Object width) {
        this.width = width;
        return this;
    }

    public Object height() {
        return this.height;
    }

    public Treemap height(Object height) {
        this.height = height;
        return this;
    }

    /**
     * @since 3.5.1
     */
    private Integer calendarIndex;

    public Integer calendarIndex() {
        return calendarIndex;
    }

    public Treemap calendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return this.label;
    }

    public Treemap label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public Treemap itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public String type() {
        return type;
    }

    public Object leafDepth() {
        return leafDepth;
    }

    public Treemap leafDepth(Object leafDepth) {
        this.leafDepth = leafDepth;
        return this;
    }

    public Object roam() {
        return roam;
    }

    public Treemap roam(Object roam) {
        this.roam = roam;
        return this;
    }

    public String nodeClick() {
        return nodeClick;
    }

    public Treemap nodeClick(String nodeClick) {
        this.nodeClick = nodeClick;
        return this;
    }

    public Double zoomToNodeRatio() {
        return zoomToNodeRatio;
    }

    public Treemap zoomToNodeRatio(Double zoomToNodeRatio) {
        this.zoomToNodeRatio = zoomToNodeRatio;
        return this;
    }

    public Level[] levels() {
        return levels;
    }

    public Treemap levels(Level... levels) {
        this.levels = levels;
        return this;
    }

    public Double squareRatio() {
        return squareRatio;
    }

    public Treemap squareRatio(Double squareRatio) {
        this.squareRatio = squareRatio;
        return this;
    }

    public Silent silent() {
        if (silent == null)
            silent = new Silent();
        return silent;
    }

    public Treemap silent(Silent silent) {
        this.silent = silent;
        return this;
    }

    public Object visualDimension() {
        return visualDimension;
    }

    public Treemap visualDimension(Integer visualDimension) {
        this.visualDimension = visualDimension;
        return this;
    }

    public Object visualMin() {
        return visualMin;
    }

    public Treemap visualMin(Object visualMin) {
        this.visualMin = visualMin;
        return this;
    }

    public Object visualMax() {
        return visualMax;
    }

    public Treemap visualMax(Object visualMax) {
        this.visualMax = visualMax;
        return this;
    }

    public Object[] colorAlpha() {
        return colorAlpha;
    }

    public Treemap colorAlpha(Object... colorAlpha) {
        this.colorAlpha = colorAlpha;
        return this;
    }

    public Double colorSaturation() {
        return colorSaturation;
    }

    public Treemap colorSaturation(Double colorSaturation) {
        this.colorSaturation = colorSaturation;
        return this;
    }

    public Object colorMappingBy() {
        return colorMappingBy;
    }

    public Treemap colorMappingBy(Object colorMappingBy) {
        this.colorMappingBy = colorMappingBy;
        return this;
    }

    public Double visibleMin() {
        return visibleMin;
    }

    public Treemap visibleMin(Double visibleMin) {
        this.visibleMin = visibleMin;
        return this;
    }

    public Double childrenVisibleMin() {
        return childrenVisibleMin;
    }

    public Treemap childrenVisibleMin(Double childrenVisibleMin) {
        this.childrenVisibleMin = childrenVisibleMin;
        return this;
    }

    public Breadcrumb breadcrumb() {
        if (breadcrumb == null)
            breadcrumb = new Breadcrumb();
        return breadcrumb;
    }

    public Treemap breadcrumb(Breadcrumb breadcrumb) {
        this.breadcrumb = breadcrumb;
        return this;
    }

    public List<Object> data() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public Treemap data(Object... values) {
        if (data == null)
            data = new ArrayList<Object>();
        this.data.add(values);
        return this;
    }

    public Object animationDuration() {
        return animationDuration;
    }

    public Treemap animationDuration(Object animationDuration) {
        this.animationDuration = animationDuration;
        return this;
    }

    public Object animationEasing() {
        return animationEasing;
    }

    public Treemap animationEasing(Object animationEasing) {
        this.animationEasing = animationEasing;
        return this;
    }

    public Object animationDelay() {
        return animationDelay;
    }

    public Treemap animationDelay(Object animationDelay) {
        this.animationDelay = animationDelay;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getSquareRatio() {
        return squareRatio;
    }

    public void setSquareRatio(Double squareRatio) {
        this.squareRatio = squareRatio;
    }

    public Object getLeafDepth() {
        return leafDepth;
    }

    public void setLeafDepth(Object leafDepth) {
        this.leafDepth = leafDepth;
    }

    public Object getRoam() {
        return roam;
    }

    public void setRoam(Object roam) {
        this.roam = roam;
    }

    public String getNodeClick() {
        return nodeClick;
    }

    public void setNodeClick(String nodeClick) {
        this.nodeClick = nodeClick;
    }

    public Double getZoomToNodeRatio() {
        return zoomToNodeRatio;
    }

    public void setZoomToNodeRatio(Double zoomToNodeRatio) {
        this.zoomToNodeRatio = zoomToNodeRatio;
    }

    public Level[] getLevels() {
        return levels;
    }

    public void setLevels(Level[] levels) {
        this.levels = levels;
    }

    public Silent getSilent() {
        return silent;
    }

    public void setSilent(Silent silent) {
        this.silent = silent;
    }

    public Integer getVisualDimension() {
        return visualDimension;
    }

    public void setVisualDimension(Integer visualDimension) {
        this.visualDimension = visualDimension;
    }

    public Object getVisualMin() {
        return visualMin;
    }

    public void setVisualMin(Object visualMin) {
        this.visualMin = visualMin;
    }

    public Object getVisualMax() {
        return visualMax;
    }

    public void setVisualMax(Object visualMax) {
        this.visualMax = visualMax;
    }

    public Object[] getColorAlpha() {
        return colorAlpha;
    }

    public void setColorAlpha(Object[] colorAlpha) {
        this.colorAlpha = colorAlpha;
    }

    public Double getColorSaturation() {
        return colorSaturation;
    }

    public void setColorSaturation(Double colorSaturation) {
        this.colorSaturation = colorSaturation;
    }

    public Object getColorMappingBy() {
        return colorMappingBy;
    }

    public void setColorMappingBy(Object colorMappingBy) {
        this.colorMappingBy = colorMappingBy;
    }

    public Double getVisibleMin() {
        return visibleMin;
    }

    public void setVisibleMin(Double visibleMin) {
        this.visibleMin = visibleMin;
    }

    public Double getChildrenVisibleMin() {
        return childrenVisibleMin;
    }

    public void setChildrenVisibleMin(Double childrenVisibleMin) {
        this.childrenVisibleMin = childrenVisibleMin;
    }

    public Breadcrumb getBreadcrumb() {
        return breadcrumb;
    }

    public void setBreadcrumb(Breadcrumb breadcrumb) {
        this.breadcrumb = breadcrumb;
    }

    public Object getAnimationDuration() {
        return animationDuration;
    }

    public void setAnimationDuration(Object animationDuration) {
        this.animationDuration = animationDuration;
    }

    public Object getAnimationEasing() {
        return animationEasing;
    }

    public void setAnimationEasing(String animationEasing) {
        this.animationEasing = animationEasing;
    }

    public Object getAnimationDelay() {
        return animationDelay;
    }

    public void setAnimationDelay(Object animationDelay) {
        this.animationDelay = animationDelay;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public void setAnimationEasing(Object animationEasing) {
        this.animationEasing = animationEasing;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Integer getCalendarIndex() {
        return calendarIndex;
    }

    public void setCalendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public String getDrillDownIcon() {
        return drillDownIcon;
    }

    public void setDrillDownIcon(String drillDownIcon) {
        this.drillDownIcon = drillDownIcon;
    }

    public LabelStyle getUpperLabel() {
        return upperLabel;
    }

    public void setUpperLabel(LabelStyle upperLabel) {
        this.upperLabel = upperLabel;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public static class ItemStyle {
        /**
         * 矩形的颜色。默认从全局调色盘 option.color 获取颜色。
         */
        private Object color;
        /**
         * 矩形颜色的透明度。取值范围是 0 ~ 1 之间的浮点数。
         */
        private Object colorAlpha;
        /**
         * 矩形颜色的饱和度。取值范围是 0 ~ 1 之间的浮点数。
         */
        private Object colorSaturation;
        /**
         * 矩形边框线宽。为 0 时无边框。而矩形的内部子矩形（子节点）的间隔距离是由 gapWidth 指定的
         */
        private Object borderWidth;
        /**
         * 矩形内部子矩形（子节点）的间隔距离。
         */
        private Object gapWidth;
        /**
         * 矩形边框 和 矩形间隔（gap）的颜色。
         */
        private Object borderColor;
        /**
         * 矩形边框的颜色的饱和度。取值范围是 0 ~ 1 之间的浮点数
         */
        private Object borderColorSaturation;
        /**
         * 每个矩形的描边颜色。
         */
        private Object strokeColor;
        /**
         * 每个矩形的描边宽度。
         */
        private Object strokeWidth;

        public Object strokeWidth() {
            return this.strokeWidth;
        }

        public ItemStyle strokeWidth(Object strokeWidth) {
            this.strokeWidth = strokeWidth;
            return this;
        }

        public Object strokeColor() {
            return this.strokeColor;
        }

        public ItemStyle strokeColor(Object strokeColor) {
            this.strokeColor = strokeColor;
            return this;
        }

        public Object borderColorSaturation() {
            return this.borderColorSaturation;
        }

        public ItemStyle borderColorSaturation(Object borderColorSaturation) {
            this.borderColorSaturation = borderColorSaturation;
            return this;
        }

        public Object borderColor() {
            return this.borderColor;
        }

        public ItemStyle borderColor(Object borderColor) {
            this.borderColor = borderColor;
            return this;
        }

        public Object gapWidth() {
            return this.gapWidth;
        }

        public ItemStyle gapWidth(Object gapWidth) {
            this.gapWidth = gapWidth;
            return this;
        }

        public Object borderWidth() {
            return this.borderWidth;
        }

        public ItemStyle borderWidth(Object borderWidth) {
            this.borderWidth = borderWidth;
            return this;
        }

        public Object colorSaturation() {
            return this.colorSaturation;
        }

        public ItemStyle colorSaturation(Object colorSaturation) {
            this.colorSaturation = colorSaturation;
            return this;
        }

        public Object colorAlpha() {
            return this.colorAlpha;
        }

        public ItemStyle colorAlpha(Object colorAlpha) {
            this.colorAlpha = colorAlpha;
            return this;
        }

        public Object color() {
            return this.color;
        }

        public ItemStyle color(Object color) {
            this.color = color;
            return this;
        }

        public Object getColor() {
            return color;
        }

        public void setColor(Object color) {
            this.color = color;
        }

        public Object getColorAlpha() {
            return colorAlpha;
        }

        public void setColorAlpha(Object colorAlpha) {
            this.colorAlpha = colorAlpha;
        }

        public Object getColorSaturation() {
            return colorSaturation;
        }

        public void setColorSaturation(Object colorSaturation) {
            this.colorSaturation = colorSaturation;
        }

        public Object getBorderWidth() {
            return borderWidth;
        }

        public void setBorderWidth(Object borderWidth) {
            this.borderWidth = borderWidth;
        }

        public Object getGapWidth() {
            return gapWidth;
        }

        public void setGapWidth(Object gapWidth) {
            this.gapWidth = gapWidth;
        }

        public Object getBorderColor() {
            return borderColor;
        }

        public void setBorderColor(Object borderColor) {
            this.borderColor = borderColor;
        }

        public Object getBorderColorSaturation() {
            return borderColorSaturation;
        }

        public void setBorderColorSaturation(Object borderColorSaturation) {
            this.borderColorSaturation = borderColorSaturation;
        }

        public Object getStrokeColor() {
            return strokeColor;
        }

        public void setStrokeColor(Object strokeColor) {
            this.strokeColor = strokeColor;
        }

        public Object getStrokeWidth() {
            return strokeWidth;
        }

        public void setStrokeWidth(Object strokeWidth) {
            this.strokeWidth = strokeWidth;
        }
    }
}
