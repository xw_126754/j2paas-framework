/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.style.TextStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Block;
import cn.easyplatform.web.ext.echarts.lib.support.PageIcon;

import java.io.Serializable;
import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Legend extends Block implements Serializable {
    /**
     * 是否显示
     */
    private Boolean show;
    /**
     * 布局方式，默认为水平布局，可选为：'horizontal' | 'vertical'
     */
    private String orient;
    /**
     * 图例标记和文本的对齐。默认自动，根据组件的位置和 orient 决定，当组件的 left 值为 'right' 以及纵向布局（orient 为 'vertical'）的时候为右对齐，及为 'right'。
     */
    private String align;
    /**
     * 标题内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距，同css，见下图
     */
    private Object padding;
    /**
     * 主副标题纵向间隔，单位px，默认为10
     */
    private Integer itemGap;
    /**
     * 图例图形宽度
     */
    private Integer itemWidth;
    /**
     * 图例图形高度
     */
    private Integer itemHeight;
    /**
     * 如果图标（可能来自系列的 symbol 或用户自定义的 legend.data.icon）是 path:// 的形式，是否在缩放时保持该图形的长宽比。
     */
    private Boolean symbolKeepAspect;
    /**
     * 用来格式化图例文本，支持字符串模板和回调函数两种形式
     */
    private String formatter;
    /**
     * 选择模式，默认开启图例开关
     */
    private Object selectedMode;
    /**
     * 图例关闭时的颜色。
     */
    private String inactiveColor;
    /**
     * 配置默认选中状态，可配合LEGEND.SELECTED事件做动态数据载入
     */
    private Map<String, Boolean> selected;
    /**
     * 文字样式
     */
    private TextStyle textStyle;
    /**
     * 图例的 tooltip 配置
     */
    private Tooltip tooltip;
    /**
     * 图例项的 icon,ECharts 提供的标记类型包括 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none'
     * <p>
     * 可以通过 'image://url' 设置为图片，其中 URL 为图片的链接，或者 dataURI。
     */
    private String icon;
    /**
     * 图例内容数组，数组项通常为{string}，每一项代表一个系列的name。
     */
    private Object data;
    /**
     * legend.type 为 'scroll' 时有效。
     *
     * 图例当前最左上显示项的 dataIndex。
     */
    private Integer scrollDataIndex;
    /**
     * legend.type 为 'scroll' 时有效。
     *
     * 图例控制块和图例项之间的间隔。
     */
    private Integer pageButtonGap;
    /**
     *legend.type 为 'scroll' 时有效。
     *
     * 图例控制块的位置。可选值为：
     * 'start'：控制块在左或上。
     * 'end'：控制块在右或下。
     */
    private String pageButtonPosition;
    /**
     * legend.type 为 'scroll' 时有效。
     *
     * 图例控制块中，页信息的显示格式。默认为 '{current}/{total}'，其中 {current} 是当前页号（从 1 开始计数），{total} 是总页数
     */
    private Object pageFormatter;
    /**
     * 图例控制块的图标
     */
    private PageIcon pageIcons;
    /**
     * legend.type 为 'scroll' 时有效。
     *
     * 翻页按钮的颜色。
     */
    private String pageIconColor;
    /**
     * 翻页按钮不激活时（即翻页到头时）的颜色
     */
    private String pageIconInactiveColor;
    /**
     * 翻页按钮的大小。可以是数字，也可以是数组，如 [10, 3]，表示 [宽，高]
     */
    private Object pageIconSize;
    /**
     * 图例页信息的文字样式
     */
    private TextStyle pageTextStyle;
    /**
     * 图例翻页是否使用动画
     */
    private Boolean animation;
    /**
     * 图例翻页时的动画时长
     */
    private Integer animationDurationUpdate;

    public Boolean show() {
        return this.show;
    }

    public Legend show(Boolean show) {
        this.show = show;
        return this;
    }

    public String orient() {
        return this.orient;
    }

    public Legend orient(String orient) {
        this.orient = orient;
        return this;
    }

    public String align() {
        return this.align;
    }

    public Legend align(String align) {
        this.align = align;
        return this;
    }

    public Object padding() {
        return this.padding;
    }

    public Legend padding(Object padding) {
        this.padding = padding;
        return this;
    }

    public Legend padding(Object... padding) {
        if (padding != null && padding.length > 4) {
            throw new RuntimeException("padding属性最多可以接收4个参数!");
        }
        this.padding = padding;
        return this;
    }

    public Integer itemGap() {
        return this.itemGap;
    }

    public Legend itemGap(Integer itemGap) {
        this.itemGap = itemGap;
        return this;
    }

    public Integer itemWidth() {
        return this.itemWidth;
    }

    public Legend itemWidth(Integer itemWidth) {
        this.itemWidth = itemWidth;
        return this;
    }

    public Integer itemHeight() {
        return this.itemHeight;
    }

    public Legend itemHeight(Integer itemHeight) {
        this.itemHeight = itemHeight;
        return this;
    }

    public String formatter() {
        return this.formatter;
    }

    public Legend formatter(String formatter) {
        this.formatter = formatter;
        return this;
    }

    public Object selectedMode() {
        return this.selectedMode;
    }

    public Legend selectedMode(Object selectedMode) {
        this.selectedMode = selectedMode;
        return this;
    }

    public String inactiveColor() {
        return this.inactiveColor;
    }

    public Legend inactiveColor(String inactiveColor) {
        this.inactiveColor = inactiveColor;
        return this;
    }

    public Boolean selected(String name) {
        if (this.selected == null) {
            return null;
        } else {
            return selected.get(name);
        }
    }

    public Legend selected(String name, Boolean selected) {
        if (this.selected == null) {
            this.selected = new LinkedHashMap<String, Boolean>();
        }
        this.selected.put(name, selected);
        return this;
    }

    public TextStyle textStyle() {
        if (this.textStyle == null) {
            this.textStyle = new TextStyle();
        }
        return this.textStyle;
    }

    public Legend textStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
        return this;
    }

    public Tooltip tooltip() {
        if (this.tooltip == null)
            this.tooltip = new Tooltip();
        return this.tooltip;
    }

    public Legend tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public String getOrient() {
        return orient;
    }

    public void setOrient(String orient) {
        this.orient = orient;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public Object getPadding() {
        return padding;
    }

    public void setPadding(Object padding) {
        this.padding = padding;
    }

    public Integer getItemGap() {
        return itemGap;
    }

    public void setItemGap(Integer itemGap) {
        this.itemGap = itemGap;
    }

    public Integer getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(Integer itemWidth) {
        this.itemWidth = itemWidth;
    }

    public Integer getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(Integer itemHeight) {
        this.itemHeight = itemHeight;
    }

    public String getFormatter() {
        return formatter;
    }

    public void setFormatter(String formatter) {
        this.formatter = formatter;
    }

    public Object getSelectedMode() {
        return selectedMode;
    }

    public void setSelectedMode(Object selectedMode) {
        this.selectedMode = selectedMode;
    }

    public String getInactiveColor() {
        return inactiveColor;
    }

    public void setInactiveColor(String inactiveColor) {
        this.inactiveColor = inactiveColor;
    }

    public Map<String, Boolean> getSelected() {
        return selected;
    }

    public void setSelected(Map<String, Boolean> selected) {
        this.selected = selected;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
