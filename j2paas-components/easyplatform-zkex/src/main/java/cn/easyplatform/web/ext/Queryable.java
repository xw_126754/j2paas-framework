/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext;

import cn.easyplatform.type.Option;

/**
 * 通过定义SQL来获取数据源
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface Queryable {
    /**
     * 查询语句，可以有多个栏位
     */
    Object getQuery();

    default void setOptionQuery(Option option) {
        if (getQuery() == null && option.getOptionType() == Option.Type.DICTIONARY && option.getOptionValue() != null) {
            String[] ovs = option.getOptionValue().split(",");
            if (ovs.length == 1) {
                setQuery("SELECT detailno,#726 FROM sys_dic_detail_info WHERE diccode='" + option.getOptionValue() + "'");
            } else if (ovs.length == 3) {
                setQuery(new StringBuilder(30).append("SELECT ").append(ovs[1]).append(",").append(ovs[2]).append(" FROM ").append(ovs[0]).toString());
            } else if (ovs.length == 4) {
                setQuery(new StringBuilder(50).append("SELECT ").append(ovs[1]).append(",").append(ovs[2]).append(" FROM ").append(ovs[0]).append(" WHERE ").append(ovs[3]).toString());
            }
        }
    }

    /**
     * 设置查询语句
     *
     * @param query
     */
    void setQuery(Object query);

    /**
     * 数据连接的资源id
     */
    String getDbId();

    /**
     * 过滤表达式
     */
    String getFilter();

}
